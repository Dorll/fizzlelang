﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace FizzleLang
{
    public class Program
    {

        public static void Main(string[] args)
        {
            // Todo: fix this shit sometime later I'm too fucking tired 
            string file = GetFile(Directory.GetCurrentDirectory());
            System.Console.WriteLine(file);
        }
        private static string GetFile(string directoryPath)
        {
            List<string> acceptedExtension = new List<string>() { "fz", "fiz", "fzz", "fze", "fizz" };
            string[] files = Directory.GetFiles(directoryPath);

            for (int i = 0; i < files.Length; i++)
            {
                string? file = files[i];
                var fileNameNoExtension = Path.GetFileNameWithoutExtension(file);
                var ext = Path.GetExtension(file);

                if (ext.Equals(acceptedExtension[i]))
                    return fileNameNoExtension + ext;

            }

            return String.Empty;
        }
    }
}